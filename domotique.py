
import sys
import os
import json
import requests
import time
import Adafruit_DHT as dht
from datetime import datetime
from time import strftime

GPIO = 4
api_url = "http://20.188.47.56/temperatures/1"

class Temperature:

    def __init__(self, url):

        r=requests.get(url)

        if r.status_code == 200 or r.status_code == 204:
            print("La connexion à l'API a bien abouti\n....................................................\n")
            json_parse = r.json() #Conversion en objet JSON
            self.ambiante = json_parse['ambiante']
            self.reglage = json_parse['reglage']

        else:
            print("La connexion à l'API a échoué\n")

def patch(self, temp, date):

    payload = {"ambiante": temp, "send_at": date}
    r=requests.patch(url, data=json.dumps(payload), headers={'Content-Type': 'application/json'})

    if r.status_code == 200 or r.status_code == 204:
        print("Mesures transmises avec succès\n")
    else:
        print("La transmission a échoué\n")


mesure = Temperature(url)
print("La température ambiante est de {}°C".format(mesure.ambiante))
print("La température de réglage est de {}°C\n".format(mesure.reglage))

float_ambiante = float(mesure.ambiante)
round_ambiante = round(float_ambiante)  #Conversion de la température ambiante à l'unité
int_reglage = int(mesure.reglage)

i=0
if round_ambiante > int_reglage:
    diff = round_ambiante - int_reglage
    print("Nous avons un écart de {}°C\n".format(diff))
    while i != diff:
        os.system('irsend SEND_ONCE CLIM KEY_VOLUMEDOWN') #Envoi du signal IR "diminue d'1 °C"
        i+=1
elif round_ambiante == int_reglage:
    pass

else:
    diff = int_reglage - round_ambiante
    print("Nous avons un écart de {}°C\n".format(diff))
    while i != diff:
        os.system('irsend SEND_ONCE CLIM KEY_VOLUMEUP') #Envoi du signal IR "augmente d'1 °C"
        i+=1

humidity, temperature = dht.read_retry(dht.DHT22, GPIO)
if temperature is not None:
    t = '{0:0.1f}'.format(temperature)
    print("La température mesurée est de {}°C\n".format(t))
    float_t = float(t)
    now = datetime.now()
    date_time = now.strftime("%Y-%m-%dT%H:%M:%S.000Z") #Conversion de la date au format imposé par l'API
    mesure.patch(float_t, date_time)
else:
    print("La lecture du DHT22 a échoué")

sys.exit(1)
